﻿using System;
using System.Windows.Forms;
using PhoneBookInWeb_WebServices3_MD5_WinForms.com.skillcoding;
using System.Threading;

namespace PhoneBookInWeb_WebServices3_MD5_WinForms
{
    delegate void DEL();
    public partial class Form1 : Form
    {
        bool flag;
        DEL del;
        public Form1()
        {
            InitializeComponent();
            del = new DEL(SetTextBox);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            flag = false;
            HASH service = new HASH();

            //GetHashStringCompleted возникнет когда, удаленный ВебМетод вернет результат. Тет запускается доп поток.
            service.GetHashStringCompleted += service_GetHashStringCompleted;
            //вызвали асинсхронно ВебМетод с помощью делегата!
            service.GetHashStringAsync(textBox1.Text.Trim());

            //Создаем доп поток для динамичности добавления " +" в textbox2
            textBox2.Text = "Шифруем";

            //метод работает уже во вторичном потоке
            Thread waitRez = new Thread(new ThreadStart(WaitHashResult));

            waitRez.Start();
        }
        private void WaitHashResult() //доп поток
        {
            while (!flag)
            {
                Invoke(del); //асинхронный вызов SetTextBox
                Thread.Sleep(10);
            }
        }
        void SetTextBox()
        {
            textBox2.Text += " +"; //textBox2 создан в основном потоке, поэтому в доп потоке WaitHashResult - вызываем Invoke(del);!!!
            this.Refresh();
        }
        void service_GetHashStringCompleted(object sender, GetHashStringCompletedEventArgs e)
        {
            flag = true;
            textBox2.Text = e.Result;
        }
    }
}
